<!DOCTYPE html>
<html>
<head>
	<title>Membuat CRUD dengan CodeIgniter</title>
</head>
<body>
	<center><h1>Membuat CRUD dengan CodeIgniter</h1></center>
	<center><?php echo anchor('crud/tambah','Tambah Data'); ?></center>
	<table style="margin:20px auto;" border="1">
		<tr>
			<th>Nama</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Action</th>
		</tr>
		<?php 
		foreach($users as $u){ 
		?>
		<tr>
			<td><?php echo $u->name ?></td>
			<td><?php echo $u->email ?></td>
			<td><?php echo $u->phone ?></td>
			<td>
			    <?php echo anchor('crud/edit/'.$u->id,'Edit'); ?>
                <?php echo anchor('crud/hapus/'.$u->id,'Hapus'); ?>
			</td>
		</tr>
		<?php } ?>
	</table>
</body>
</html>